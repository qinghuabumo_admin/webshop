namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HuiFu")]
    public partial class HuiFu
    {
        [Key]
        [StringLength(32)]
        public string H_no { get; set; }

        [Required]
        [StringLength(20)]
        public string H_writer { get; set; }

        [Required]
        [StringLength(32)]
        public string PL_no { get; set; }

        [Required]
        [StringLength(200)]
        public string H_content { get; set; }

        public int H_flag { get; set; }

        public DateTime H_time { get; set; }

        public virtual PingLun PingLun { get; set; }
    }
}
