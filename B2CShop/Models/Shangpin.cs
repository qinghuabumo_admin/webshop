namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Shangpin")]
    public partial class Shangpin
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Shangpin()
        {
            shangpinshoucang = new HashSet<shangpinshoucang>();
        }

        [Required]
        [StringLength(32)]
        public string S_no { get; set; }

        [Required]
        [StringLength(30)]
        [Display(Name = "��Ʒ����")]
        public string S_name { get; set; }

        [Key]
        [StringLength(5)]

        public string S_number { get; set; }

        public int C_no { get; set; }

        public int B_no { get; set; }

        [Column(TypeName = "money")]
        public decimal S_costprice { get; set; }

        [Column(TypeName = "money")]
        public decimal S_marketprice { get; set; }

        [Column(TypeName = "money")]
        public decimal S_myprice { get; set; }

        public decimal S_weight { get; set; }

        public int S_count { get; set; }

        public int S_max { get; set; }

        public int S_min { get; set; }

        public int S_flag { get; set; }

        public int S_label { get; set; }

        public int S_isHot { get; set; }

        public int S_order { get; set; }

        public virtual Brand Brand { get; set; }

        public virtual Classfiy Classfiy { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<shangpinshoucang> shangpinshoucang { get; set; }
    }
}
