namespace B2CShop.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class B2C : DbContext
    {
        public B2C()
            : base("name=B2C123")
        {
        }

        public virtual DbSet<Admin> Admin { get; set; }
        public virtual DbSet<Attribute> Attribute { get; set; }
        public virtual DbSet<AttributeGroup> AttributeGroup { get; set; }
        public virtual DbSet<AttributeValue> AttributeValue { get; set; }
        public virtual DbSet<Brand> Brand { get; set; }
        public virtual DbSet<BrandClass> BrandClass { get; set; }
        public virtual DbSet<Classfiy> Classfiy { get; set; }
        public virtual DbSet<Cuxiao> Cuxiao { get; set; }
        public virtual DbSet<Distribution> Distribution { get; set; }
        public virtual DbSet<Favorable> Favorable { get; set; }
        public virtual DbSet<FavorableShop> FavorableShop { get; set; }
        public virtual DbSet<FavorableUser> FavorableUser { get; set; }
        public virtual DbSet<Gift> Gift { get; set; }
        public virtual DbSet<HuiFu> HuiFu { get; set; }
        public virtual DbSet<OrderDetailed> OrderDetailed { get; set; }
        public virtual DbSet<Orders> Orders { get; set; }
        public virtual DbSet<Pay> Pay { get; set; }
        public virtual DbSet<PingLun> PingLun { get; set; }
        public virtual DbSet<rizhi> rizhi { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<RoleUser> RoleUser { get; set; }
        public virtual DbSet<Shangpin> Shangpin { get; set; }
        public virtual DbSet<ShangpinImg> ShangpinImg { get; set; }
        public virtual DbSet<ShangpinRelation> ShangpinRelation { get; set; }
        public virtual DbSet<shangpinshoucang> shangpinshoucang { get; set; }
        public virtual DbSet<ShangpinTiming> ShangpinTiming { get; set; }
        public virtual DbSet<ShopCart> ShopCart { get; set; }
        public virtual DbSet<Tjifen> Tjifen { get; set; }
        public virtual DbSet<TuiKuan> TuiKuan { get; set; }
        public virtual DbSet<UserAddress> UserAddress { get; set; }
        public virtual DbSet<Userinfo> Userinfo { get; set; }
        public virtual DbSet<UserType> UserType { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Admin>()
                .Property(e => e.A_uid)
                .IsUnicode(false);

            modelBuilder.Entity<Admin>()
                .Property(e => e.A_pwd)
                .IsUnicode(false);

            modelBuilder.Entity<Admin>()
                .HasMany(e => e.RoleUser)
                .WithRequired(e => e.Admin)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Attribute>()
                .Property(e => e.A_name)
                .IsUnicode(false);

            modelBuilder.Entity<Attribute>()
                .HasMany(e => e.AttributeValue)
                .WithRequired(e => e.Attribute)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AttributeGroup>()
                .Property(e => e.AG_name)
                .IsUnicode(false);

            modelBuilder.Entity<AttributeGroup>()
                .HasMany(e => e.Attribute)
                .WithRequired(e => e.AttributeGroup)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AttributeValue>()
                .Property(e => e.AV_name)
                .IsUnicode(false);

            modelBuilder.Entity<Brand>()
                .Property(e => e.B_name)
                .IsUnicode(false);

            modelBuilder.Entity<Brand>()
                .Property(e => e.B_Log)
                .IsUnicode(false);

            modelBuilder.Entity<Brand>()
                .HasMany(e => e.BrandClass)
                .WithRequired(e => e.Brand)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Brand>()
                .HasMany(e => e.Shangpin)
                .WithRequired(e => e.Brand)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Classfiy>()
                .Property(e => e.C_name)
                .IsUnicode(false);

            modelBuilder.Entity<Classfiy>()
                .Property(e => e.C_prices)
                .IsUnicode(false);

            modelBuilder.Entity<Classfiy>()
                .HasMany(e => e.AttributeGroup)
                .WithRequired(e => e.Classfiy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Classfiy>()
                .HasMany(e => e.BrandClass)
                .WithRequired(e => e.Classfiy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Classfiy>()
                .HasMany(e => e.Classfiy1)
                .WithOptional(e => e.Classfiy2)
                .HasForeignKey(e => e.C_cno);

            modelBuilder.Entity<Classfiy>()
                .HasMany(e => e.Shangpin)
                .WithRequired(e => e.Classfiy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cuxiao>()
                .Property(e => e.C_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Cuxiao>()
                .Property(e => e.S_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Cuxiao>()
                .HasMany(e => e.Gift)
                .WithRequired(e => e.Cuxiao)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Distribution>()
                .Property(e => e.D_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Distribution>()
                .Property(e => e.D_money)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Distribution>()
                .Property(e => e.D_methood)
                .IsUnicode(false);

            modelBuilder.Entity<Distribution>()
                .HasMany(e => e.Orders)
                .WithRequired(e => e.Distribution)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Favorable>()
                .Property(e => e.F_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Favorable>()
                .Property(e => e.F_name)
                .IsUnicode(false);

            modelBuilder.Entity<Favorable>()
                .Property(e => e.F_money)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Favorable>()
                .Property(e => e.F_min)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Favorable>()
                .HasMany(e => e.FavorableShop)
                .WithRequired(e => e.Favorable)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Favorable>()
                .HasMany(e => e.FavorableUser)
                .WithRequired(e => e.Favorable)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Favorable>()
                .HasMany(e => e.OrderDetailed)
                .WithRequired(e => e.Favorable)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FavorableShop>()
                .Property(e => e.F_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<FavorableShop>()
                .Property(e => e.S_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<FavorableUser>()
                .Property(e => e.F_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Gift>()
                .Property(e => e.C_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Gift>()
                .Property(e => e.S_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<HuiFu>()
                .Property(e => e.H_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<HuiFu>()
                .Property(e => e.H_writer)
                .IsUnicode(false);

            modelBuilder.Entity<HuiFu>()
                .Property(e => e.PL_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<HuiFu>()
                .Property(e => e.H_content)
                .IsUnicode(false);

            modelBuilder.Entity<OrderDetailed>()
                .Property(e => e.OD_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OrderDetailed>()
                .Property(e => e.S_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OrderDetailed>()
                .Property(e => e.OD_number)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OrderDetailed>()
                .Property(e => e.OD_attribute)
                .IsUnicode(false);

            modelBuilder.Entity<OrderDetailed>()
                .Property(e => e.OD_price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderDetailed>()
                .Property(e => e.OD_name)
                .IsUnicode(false);

            modelBuilder.Entity<OrderDetailed>()
                .Property(e => e.F_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OrderDetailed>()
                .Property(e => e.OD_saleprice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderDetailed>()
                .HasMany(e => e.Orders)
                .WithRequired(e => e.OrderDetailed)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OrderDetailed>()
                .HasMany(e => e.TuiKuan)
                .WithRequired(e => e.OrderDetailed)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Orders>()
                .Property(e => e.O_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Orders>()
                .Property(e => e.D_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Orders>()
                .Property(e => e.P_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Orders>()
                .Property(e => e.O_sum)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Orders>()
                .Property(e => e.OD_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Pay>()
                .Property(e => e.P_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Pay>()
                .HasMany(e => e.Orders)
                .WithRequired(e => e.Pay)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PingLun>()
                .Property(e => e.PL_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PingLun>()
                .Property(e => e.S_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PingLun>()
                .Property(e => e.PL_content)
                .IsUnicode(false);

            modelBuilder.Entity<PingLun>()
                .HasMany(e => e.HuiFu)
                .WithRequired(e => e.PingLun)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Role>()
                .Property(e => e.R_name)
                .IsUnicode(false);

            modelBuilder.Entity<Role>()
                .Property(e => e.R_display)
                .IsUnicode(false);

            modelBuilder.Entity<Role>()
                .HasMany(e => e.RoleUser)
                .WithRequired(e => e.Role)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<RoleUser>()
                .Property(e => e.A_uid)
                .IsUnicode(false);

            modelBuilder.Entity<Shangpin>()
                .Property(e => e.S_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Shangpin>()
                .Property(e => e.S_number)
                .IsUnicode(false);

            modelBuilder.Entity<Shangpin>()
                .Property(e => e.S_costprice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Shangpin>()
                .Property(e => e.S_marketprice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Shangpin>()
                .Property(e => e.S_myprice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Shangpin>()
                .Property(e => e.S_weight)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Shangpin>()
                .HasMany(e => e.shangpinshoucang)
                .WithRequired(e => e.Shangpin)
                .HasForeignKey(e => e.spid)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ShangpinImg>()
                .Property(e => e.S_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ShangpinImg>()
                .Property(e => e.SI_img)
                .IsUnicode(false);

            modelBuilder.Entity<ShangpinRelation>()
                .Property(e => e.SR_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ShangpinRelation>()
                .Property(e => e.S_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ShangpinRelation>()
                .Property(e => e.SR_sno)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<shangpinshoucang>()
                .Property(e => e.spid)
                .IsUnicode(false);

            modelBuilder.Entity<ShangpinTiming>()
                .Property(e => e.ST_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ShangpinTiming>()
                .Property(e => e.S_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ShopCart>()
                .Property(e => e.SC_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ShopCart>()
                .Property(e => e.S_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ShopCart>()
                .Property(e => e.SC_sname)
                .IsUnicode(false);

            modelBuilder.Entity<ShopCart>()
                .Property(e => e.SC_sprice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Tjifen>()
                .Property(e => e.yuanyin)
                .IsUnicode(false);

            modelBuilder.Entity<TuiKuan>()
                .Property(e => e.OD_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<TuiKuan>()
                .Property(e => e.TK_money)
                .HasPrecision(19, 4);

            modelBuilder.Entity<UserAddress>()
                .Property(e => e.UA_emailno)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<UserAddress>()
                .HasMany(e => e.Orders)
                .WithRequired(e => e.UserAddress)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserAddress>()
                .HasMany(e => e.Userinfo)
                .WithRequired(e => e.UserAddress)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Userinfo>()
                .HasMany(e => e.FavorableUser)
                .WithRequired(e => e.Userinfo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Userinfo>()
                .HasMany(e => e.Orders)
                .WithRequired(e => e.Userinfo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Userinfo>()
                .HasMany(e => e.PingLun)
                .WithRequired(e => e.Userinfo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Userinfo>()
                .HasMany(e => e.shangpinshoucang)
                .WithRequired(e => e.Userinfo)
                .HasForeignKey(e => e.username)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Userinfo>()
                .HasMany(e => e.ShopCart)
                .WithRequired(e => e.Userinfo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Userinfo>()
                .HasMany(e => e.Tjifen)
                .WithRequired(e => e.Userinfo)
                .HasForeignKey(e => e.username)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserType>()
                .HasMany(e => e.Cuxiao)
                .WithRequired(e => e.UserType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserType>()
                .HasMany(e => e.Userinfo)
                .WithRequired(e => e.UserType)
                .WillCascadeOnDelete(false);
        }
    }
}
