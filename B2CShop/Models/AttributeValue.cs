namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AttributeValue")]
    public partial class AttributeValue
    {
        [Key]
        public int AV_no { get; set; }

        [Required]
        [StringLength(20)]
        public string AV_name { get; set; }

        public int A_no { get; set; }

        public int A_order { get; set; }

        public virtual Attribute Attribute { get; set; }
    }
}
