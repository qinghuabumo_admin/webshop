namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ShangpinImg")]
    public partial class ShangpinImg
    {
        [Key]
        public int SI_no { get; set; }

        [Required]
        [StringLength(32)]
        public string S_no { get; set; }

        [Required]
        [StringLength(20)]
        public string SI_img { get; set; }

        public int SI_main { get; set; }

        public int SI_order { get; set; }
    }
}
