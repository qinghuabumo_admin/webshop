namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TuiKuan")]
    public partial class TuiKuan
    {
        [Required]
        [StringLength(32)]
        public string OD_no { get; set; }

        [Key]
        public int TK_no { get; set; }

        public DateTime TK_SQTime { get; set; }

        public DateTime TK_Time { get; set; }

        [Column(TypeName = "money")]
        public decimal TK_money { get; set; }

        public int TK_ZT { get; set; }

        public virtual OrderDetailed OrderDetailed { get; set; }
    }
}
