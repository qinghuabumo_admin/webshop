namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("shangpinshoucang")]
    public partial class shangpinshoucang
    {
        public int id { get; set; }

        [Required]
        [StringLength(5)]
        public string spid { get; set; }

        [Required]
        [StringLength(10)]
        public string username { get; set; }

        public DateTime createtime { get; set; }

        public virtual Shangpin Shangpin { get; set; }

        public virtual Userinfo Userinfo { get; set; }
    }
}
