namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Classfiy")]
    public partial class Classfiy
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Classfiy()
        {
            AttributeGroup = new HashSet<AttributeGroup>();
            BrandClass = new HashSet<BrandClass>();
            Classfiy1 = new HashSet<Classfiy>();
            Shangpin = new HashSet<Shangpin>();
        }

        [Key]
        public int C_no { get; set; }

        [Required]
        [StringLength(20)]
        public string C_name { get; set; }

        [Required]
        [StringLength(20)]
        public string C_prices { get; set; }

        public int? C_cno { get; set; }

        public int C_order { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AttributeGroup> AttributeGroup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BrandClass> BrandClass { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Classfiy> Classfiy1 { get; set; }

        public virtual Classfiy Classfiy2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Shangpin> Shangpin { get; set; }
    }
}
