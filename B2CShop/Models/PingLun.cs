namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PingLun")]
    public partial class PingLun
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PingLun()
        {
            HuiFu = new HashSet<HuiFu>();
        }

        [Key]
        [StringLength(32)]
        public string PL_no { get; set; }

        [Required]
        [StringLength(10)]
        public string U_username { get; set; }

        [Required]
        [StringLength(32)]
        public string S_no { get; set; }

        [Required]
        [StringLength(200)]
        public string PL_content { get; set; }

        public DateTime PL_time { get; set; }

        public int PL_flag { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HuiFu> HuiFu { get; set; }

        public virtual Userinfo Userinfo { get; set; }
    }
}
