namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OrderDetailed")]
    public partial class OrderDetailed
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OrderDetailed()
        {
            Orders = new HashSet<Orders>();
            TuiKuan = new HashSet<TuiKuan>();
        }

        [Key]
        [StringLength(32)]
        public string OD_no { get; set; }

        [Required]
        [StringLength(32)]
        public string S_no { get; set; }

        public int OD_count { get; set; }

        [Required]
        [StringLength(5)]
        public string OD_number { get; set; }

        [Required]
        [StringLength(10)]
        public string OD_attribute { get; set; }

        [Column(TypeName = "money")]
        public decimal OD_price { get; set; }

        [Required]
        [StringLength(20)]
        public string OD_name { get; set; }

        [Required]
        [StringLength(32)]
        public string F_no { get; set; }

        [Column(TypeName = "money")]
        public decimal OD_saleprice { get; set; }

        public virtual Favorable Favorable { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Orders> Orders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TuiKuan> TuiKuan { get; set; }
    }
}
