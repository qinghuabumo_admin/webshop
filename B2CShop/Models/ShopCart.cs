namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ShopCart")]
    public partial class ShopCart
    {
        [Key]
        [StringLength(32)]
        public string SC_no { get; set; }

        [Required]
        [StringLength(10)]
        public string U_username { get; set; }

        [Required]
        [StringLength(32)]
        public string S_no { get; set; }

        [Required]
        [StringLength(20)]
        public string SC_sname { get; set; }

        [Column(TypeName = "money")]
        public decimal SC_sprice { get; set; }

        public int S_countmax { get; set; }

        public int S_count { get; set; }

        public virtual Userinfo Userinfo { get; set; }
    }
}
