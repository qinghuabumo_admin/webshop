namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AttributeGroup")]
    public partial class AttributeGroup
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AttributeGroup()
        {
            Attribute = new HashSet<Attribute>();
        }

        [Key]
        public int AG_no { get; set; }

        [Required]
        [StringLength(20)]
        public string AG_name { get; set; }

        public int C_no { get; set; }

        public int AG_order { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Attribute> Attribute { get; set; }

        public virtual Classfiy Classfiy { get; set; }
    }
}
