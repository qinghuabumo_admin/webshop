namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Orders
    {
        [Key]
        [StringLength(32)]
        public string O_no { get; set; }

        public DateTime O_time { get; set; }

        [Required]
        [StringLength(10)]
        public string U_username { get; set; }

        public int O_flag { get; set; }

        [Required]
        [StringLength(32)]
        public string D_no { get; set; }

        [Required]
        [StringLength(32)]
        public string P_no { get; set; }

        public int UA_no { get; set; }

        public int? O_jfmoney { get; set; }

        [Column(TypeName = "money")]
        public decimal O_sum { get; set; }

        [Required]
        [StringLength(32)]
        public string OD_no { get; set; }

        public DateTime? O_goodtime { get; set; }

        public virtual Distribution Distribution { get; set; }

        public virtual OrderDetailed OrderDetailed { get; set; }

        public virtual Pay Pay { get; set; }

        public virtual Userinfo Userinfo { get; set; }

        public virtual UserAddress UserAddress { get; set; }
    }
}
