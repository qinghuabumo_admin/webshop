namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ShangpinRelation")]
    public partial class ShangpinRelation
    {
        [Key]
        [StringLength(32)]
        public string SR_no { get; set; }

        [Required]
        [StringLength(32)]
        public string S_no { get; set; }

        [Required]
        [StringLength(32)]
        public string SR_sno { get; set; }
    }
}
