namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BrandClass")]
    public partial class BrandClass
    {
        [Key]
        public int BC_no { get; set; }

        public int B_no { get; set; }

        public int C_no { get; set; }

        public virtual Brand Brand { get; set; }

        public virtual Classfiy Classfiy { get; set; }
    }
}
