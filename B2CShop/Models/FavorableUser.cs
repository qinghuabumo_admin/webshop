namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FavorableUser")]
    public partial class FavorableUser
    {
        [Key]
        public int FU_no { get; set; }

        [Required]
        [StringLength(32)]
        public string F_no { get; set; }

        [Required]
        [StringLength(10)]
        public string U_username { get; set; }

        public int FU_flag { get; set; }

        public virtual Favorable Favorable { get; set; }

        public virtual Userinfo Userinfo { get; set; }
    }
}
