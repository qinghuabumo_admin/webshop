namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("rizhi")]
    public partial class rizhi
    {
        public int id { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "操作")]
        public string caozuo { get; set; }
        [Display(Name = "创建时间")]

        public DateTime createtime { get; set; }

        [Required]
        [StringLength(20)]
        [Display(Name = "用户名")]
        public string username { get; set; }
    }
}
