namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserType")]
    public partial class UserType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public UserType()
        {
            Cuxiao = new HashSet<Cuxiao>();
            Userinfo = new HashSet<Userinfo>();
        }

        [Key]
        public int UT_no { get; set; }

        [Required]
        [StringLength(20)]
        public string UT_name { get; set; }

        [Required]
        [StringLength(30)]
        public string UT_display { get; set; }

        [Required]
        [StringLength(30)]
        public string UT_img { get; set; }

        public int UT_jfmax { get; set; }

        public int UT_jfmin { get; set; }

        public int UT_rank { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cuxiao> Cuxiao { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Userinfo> Userinfo { get; set; }
    }
}
