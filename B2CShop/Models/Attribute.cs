namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Attribute")]
    public partial class Attribute
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Attribute()
        {
            AttributeValue = new HashSet<AttributeValue>();
        }

        [Key]
        public int A_no { get; set; }

        [Required]
        [StringLength(20)]
        public string A_name { get; set; }

        public int AG_no { get; set; }

        public int A_type { get; set; }

        public int A_screen { get; set; }

        public int A_order { get; set; }

        public virtual AttributeGroup AttributeGroup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AttributeValue> AttributeValue { get; set; }
    }
}
