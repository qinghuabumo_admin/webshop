namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RoleUser")]
    public partial class RoleUser
    {
        [Key]
        public int RU_no { get; set; }

        [Required]
        [StringLength(10)]
        public string A_uid { get; set; }

        public int R_no { get; set; }

        public virtual Admin Admin { get; set; }

        public virtual Role Role { get; set; }
    }
}
