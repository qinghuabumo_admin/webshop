namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Userinfo")]
    public partial class Userinfo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Userinfo()
        {
            FavorableUser = new HashSet<FavorableUser>();
            Orders = new HashSet<Orders>();
            PingLun = new HashSet<PingLun>();
            shangpinshoucang = new HashSet<shangpinshoucang>();
            ShopCart = new HashSet<ShopCart>();
            Tjifen = new HashSet<Tjifen>();
        }

        [Key]
        [StringLength(10)]
        [Display(Name = "�û���")]
        public string U_username { get; set; }

        [Required]
        [StringLength(20)]
        [Display(Name = "����")]
        public string U_password { get; set; }

        public int UT_no { get; set; }

        public int UA_no { get; set; }

        [Required]
        [StringLength(20)]
        public string U_email { get; set; }

        [Required]
        [StringLength(20)]
        public string U_img { get; set; }

        [Required]
        [StringLength(15)]
        public string U_tel { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FavorableUser> FavorableUser { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Orders> Orders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PingLun> PingLun { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<shangpinshoucang> shangpinshoucang { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ShopCart> ShopCart { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tjifen> Tjifen { get; set; }

        public virtual UserAddress UserAddress { get; set; }

        public virtual UserType UserType { get; set; }
    }
}
