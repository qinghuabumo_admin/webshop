namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Tjifen")]
    public partial class Tjifen
    {
        public int id { get; set; }
        [Display(Name = "积分")]
        public int jifen { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "来源")]
        public string yuanyin { get; set; }

        [Display(Name = "创建时间")]
        public DateTime createtime { get; set; }

        [Required]
        [StringLength(10)]
        [Display(Name = "用户名")]
        public string username { get; set; }

        public virtual Userinfo Userinfo { get; set; }
    }
}
