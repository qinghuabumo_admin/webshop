namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Favorable")]
    public partial class Favorable
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Favorable()
        {
            FavorableShop = new HashSet<FavorableShop>();
            FavorableUser = new HashSet<FavorableUser>();
            OrderDetailed = new HashSet<OrderDetailed>();
        }

        [Key]
        [StringLength(32)]
        [Display(Name = "优惠券编号")]
        public string F_no { get; set; }

        [Required]
        [StringLength(20)]
        [Display(Name = "优惠券名称")]
        public string F_name { get; set; }

        [Column(TypeName = "money")]
        [Display(Name = "优惠券金额")]
        public decimal F_money { get; set; }

        [Column(TypeName = "money")]
        [Display(Name = "最低消费金额")]
        public decimal F_min { get; set; }

        [Display(Name = "剩余数量")]
        public int F_count { get; set; }

        [Display(Name = "使用数量")]
        public int F_userCount { get; set; }

        [Display(Name = "领取数量")]
        public int F_getCount { get; set; }

        [Display(Name = "使用开始时间")]
        public DateTime F_userstarttime { get; set; }

        [Display(Name = "试用结束时间")]
        public DateTime F_userendtime { get; set; }

        [Display(Name = "是否发放")]
        public int F_flag { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FavorableShop> FavorableShop { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FavorableUser> FavorableUser { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderDetailed> OrderDetailed { get; set; }
    }
}
