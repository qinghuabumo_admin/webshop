namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ShangpinTiming")]
    public partial class ShangpinTiming
    {
        [Key]
        [StringLength(32)]
        public string ST_no { get; set; }

        [Required]
        [StringLength(32)]
        public string S_no { get; set; }

        public DateTime ST_starttime { get; set; }

        public DateTime ST_endtime { get; set; }
    }
}
