namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserAddress")]
    public partial class UserAddress
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public UserAddress()
        {
            Orders = new HashSet<Orders>();
            Userinfo = new HashSet<Userinfo>();
        }

        [Key]
        public int UA_no { get; set; }

        [Required]
        [StringLength(30)]
        public string UA_name { get; set; }

        [Required]
        [StringLength(10)]
        public string U_username { get; set; }

        [Required]
        [StringLength(30)]
        public string UA_person { get; set; }

        public int UA_main { get; set; }

        [Required]
        [StringLength(15)]
        public string UA_tel { get; set; }

        [Required]
        [StringLength(5)]
        public string UA_emailno { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Orders> Orders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Userinfo> Userinfo { get; set; }
    }
}
