namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FavorableShop")]
    public partial class FavorableShop
    {
        [Key]
        public int FS_no { get; set; }

        [Required]
        [StringLength(32)]
        public string F_no { get; set; }

        [Required]
        [StringLength(32)]
        public string S_no { get; set; }

        public virtual Favorable Favorable { get; set; }
    }
}
