namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Distribution")]
    public partial class Distribution
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Distribution()
        {
            Orders = new HashSet<Orders>();
        }

        [Key]
        [StringLength(32)]
        public string D_no { get; set; }

        public DateTime? D_time { get; set; }

        [Column(TypeName = "money")]
        public decimal D_money { get; set; }

        [Required]
        [StringLength(30)]
        public string D_methood { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Orders> Orders { get; set; }
    }
}
