namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Cuxiao")]
    public partial class Cuxiao
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Cuxiao()
        {
            Gift = new HashSet<Gift>();
        }

        [Key]
        [StringLength(32)]
        public string C_no { get; set; }

        [Required]
        [StringLength(32)]
        public string S_no { get; set; }

        public DateTime C_starttime { get; set; }

        public DateTime C_endtime { get; set; }

        public int UT_no { get; set; }

        public int C_max { get; set; }

        public int C_flag { get; set; }

        public virtual UserType UserType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Gift> Gift { get; set; }
    }
}
