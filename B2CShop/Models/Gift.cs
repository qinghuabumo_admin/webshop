namespace B2CShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Gift")]
    public partial class Gift
    {
        [Key]
        public int G_no { get; set; }

        [Required]
        [StringLength(32)]
        public string C_no { get; set; }

        [Required]
        [StringLength(32)]
        public string S_no { get; set; }

        public int G_count { get; set; }

        public virtual Cuxiao Cuxiao { get; set; }
    }
}
