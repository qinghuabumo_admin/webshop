﻿using B2CShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace B2CShop.Controllers
{
    public class LoginController : Controller
    {
        private B2C b2c = new B2C();
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }
        // Post: Login
        [HttpPost]
        public ActionResult Index(string name,string pwd)
        {
            Userinfo ui = b2c.Userinfo.FirstOrDefault(p => p.U_password == pwd && p.U_username == name);
            if (ui != null)
            {
                Session.Add("username", ui.U_username);
                //登录成功
                return RedirectToAction("Ok");
            }
            else
            {
                //登录失败
                return RedirectToAction("Error");
            };
        }
        public ActionResult Ok()
        {
            return View();
        }
        public ActionResult Error()
        {
            return View();
        }
    }
}