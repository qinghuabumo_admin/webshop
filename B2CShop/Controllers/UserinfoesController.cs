﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using B2CShop.Models;

namespace B2CShop.Controllers
{
    public class UserinfoesController : Controller
    {
        private B2C db = new B2C();

        // GET: Userinfoes
        public ActionResult Index()
        {
            var userinfo = db.Userinfo.Include(u => u.UserAddress).Include(u => u.UserType);
            return View(userinfo.ToList());
        }
        public ActionResult Jifen()
        {
            string name = Session["username"].ToString();
            Userinfo jifen = db.Userinfo.Include(O=>O.Tjifen).FirstOrDefault(p=>p.U_username==name);
            if (jifen == null)
            {
                return View(new List<Tjifen>());
            }
            else
            {
                return View(jifen.Tjifen.ToList());
            }
            
        }
        // GET: Userinfoes/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Userinfo userinfo = db.Userinfo.Find(id);
            if (userinfo == null)
            {
                return HttpNotFound();
            }
            return View(userinfo);
        }

        // GET: Userinfoes/Create
        public ActionResult Create()
        {
            ViewBag.UA_no = new SelectList(db.UserAddress, "UA_no", "UA_name");
            ViewBag.UT_no = new SelectList(db.UserType, "UT_no", "UT_name");
            return View();
        }

        // POST: Userinfoes/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "U_username,U_password,UT_no,UA_no,U_email,U_img,U_tel")] Userinfo userinfo)
        {
            if (ModelState.IsValid)
            {
                db.Userinfo.Add(userinfo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UA_no = new SelectList(db.UserAddress, "UA_no", "UA_name", userinfo.UA_no);
            ViewBag.UT_no = new SelectList(db.UserType, "UT_no", "UT_name", userinfo.UT_no);
            return View(userinfo);
        }

        // GET: Userinfoes/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Userinfo userinfo = db.Userinfo.Find(id);
            if (userinfo == null)
            {
                return HttpNotFound();
            }
            ViewBag.UA_no = new SelectList(db.UserAddress, "UA_no", "UA_name", userinfo.UA_no);
            ViewBag.UT_no = new SelectList(db.UserType, "UT_no", "UT_name", userinfo.UT_no);
            return View(userinfo);
        }

        // POST: Userinfoes/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "U_username,U_password,UT_no,UA_no,U_email,U_img,U_tel")] Userinfo userinfo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userinfo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UA_no = new SelectList(db.UserAddress, "UA_no", "UA_name", userinfo.UA_no);
            ViewBag.UT_no = new SelectList(db.UserType, "UT_no", "UT_name", userinfo.UT_no);
            return View(userinfo);
        }

        // GET: Userinfoes/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Userinfo userinfo = db.Userinfo.Find(id);
            if (userinfo == null)
            {
                return HttpNotFound();
            }
            return View(userinfo);
        }

        // POST: Userinfoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Userinfo userinfo = db.Userinfo.Find(id);
            db.Userinfo.Remove(userinfo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        // POST: Userinfoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult addsc(string id)
        {
            string str = Session["username"].ToString();
            db.shangpinshoucang.Add(new shangpinshoucang() { createtime = DateTime.Now,spid = id,username = str });
            db.SaveChanges();
            return RedirectToAction("spsc/Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
