﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using B2CShop.Models;

namespace B2CShop.Controllers
{
    public class spscController : Controller
    {
        private B2C db = new B2C();

        // GET: spsc
        public ActionResult Index()
        {
            var shangpinshoucang = db.shangpinshoucang.Include(s => s.Shangpin).Include(s => s.Userinfo);
            return View(shangpinshoucang.ToList());
        }

        // GET: spsc/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            shangpinshoucang shangpinshoucang = db.shangpinshoucang.Find(id);
            if (shangpinshoucang == null)
            {
                return HttpNotFound();
            }
            return View(shangpinshoucang);
        }

        // GET: spsc/Create
        public ActionResult Create()
        {
            ViewBag.spid = new SelectList(db.Shangpin, "S_no", "S_name");
            ViewBag.username = new SelectList(db.Userinfo, "U_username", "U_password");
            return View();
        }

        // POST: spsc/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,spid,username,createtime")] shangpinshoucang shangpinshoucang)
        {
            if (ModelState.IsValid)
            {
                db.shangpinshoucang.Add(shangpinshoucang);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.spid = new SelectList(db.Shangpin, "S_no", "S_name", shangpinshoucang.spid);
            ViewBag.username = new SelectList(db.Userinfo, "U_username", "U_password", shangpinshoucang.username);
            return View(shangpinshoucang);
        }

        // GET: spsc/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            shangpinshoucang shangpinshoucang = db.shangpinshoucang.Find(id);
            if (shangpinshoucang == null)
            {
                return HttpNotFound();
            }
            ViewBag.spid = new SelectList(db.Shangpin, "S_no", "S_name", shangpinshoucang.spid);
            ViewBag.username = new SelectList(db.Userinfo, "U_username", "U_password", shangpinshoucang.username);
            return View(shangpinshoucang);
        }

        // POST: spsc/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,spid,username,createtime")] shangpinshoucang shangpinshoucang)
        {
            if (ModelState.IsValid)
            {
                db.Entry(shangpinshoucang).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.spid = new SelectList(db.Shangpin, "S_no", "S_name", shangpinshoucang.spid);
            ViewBag.username = new SelectList(db.Userinfo, "U_username", "U_password", shangpinshoucang.username);
            return View(shangpinshoucang);
        }

        // GET: spsc/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            shangpinshoucang shangpinshoucang = db.shangpinshoucang.Find(id);
            if (shangpinshoucang == null)
            {
                return HttpNotFound();
            }
            return View(shangpinshoucang);
        }

        // POST: spsc/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            shangpinshoucang shangpinshoucang = db.shangpinshoucang.Find(id);
            db.shangpinshoucang.Remove(shangpinshoucang);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
