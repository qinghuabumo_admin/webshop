﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using B2CShop.Models;

namespace B2CShop.Controllers
{
    public class FavorablesController : Controller
    {
        private B2C db = new B2C();

        // GET: Favorables
        public ActionResult Index()
        {
            string name = Session["username"].ToString();
           
            return View(db.FavorableUser.Where(p => p.U_username == name).Select(p => p.Favorable).ToList());
        }

        // GET: Favorables/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Favorable favorable = db.Favorable.Find(id);
            if (favorable == null)
            {
                return HttpNotFound();
            }
            return View(favorable);
        }

        // GET: Favorables/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Favorables/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "F_no,F_name,F_money,F_min,F_count,F_userCount,F_getCount,F_userstarttime,F_userendtime,F_flag")] Favorable favorable)
        {
            if (ModelState.IsValid)
            {
                db.Favorable.Add(favorable);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(favorable);
        }

        // GET: Favorables/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Favorable favorable = db.Favorable.Find(id);
            if (favorable == null)
            {
                return HttpNotFound();
            }
            return View(favorable);
        }

        // POST: Favorables/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "F_no,F_name,F_money,F_min,F_count,F_userCount,F_getCount,F_userstarttime,F_userendtime,F_flag")] Favorable favorable)
        {
            if (ModelState.IsValid)
            {
                db.Entry(favorable).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(favorable);
        }

        // GET: Favorables/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Favorable favorable = db.Favorable.Find(id);
            if (favorable == null)
            {
                return HttpNotFound();
            }
            return View(favorable);
        }

        // POST: Favorables/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Favorable favorable = db.Favorable.Find(id);
            db.Favorable.Remove(favorable);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
