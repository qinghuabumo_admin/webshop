﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using B2CShop.Models;

namespace B2CShop.Controllers
{
    public class ShangpinsController : Controller
    {
        private B2C db = new B2C();

        // GET: Shangpins
        public ActionResult Index()
        {
            var shangpin = db.Shangpin.Include(s => s.Brand).Include(s => s.Classfiy);
            return View(shangpin.ToList());
        }
        // GET: Shangpins/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shangpin shangpin = db.Shangpin.Find(id);
            if (shangpin == null)
            {
                return HttpNotFound();
            }
            return View(shangpin);
        }

        // GET: Shangpins/Create
        public ActionResult Create()
        {
            ViewBag.B_no = new SelectList(db.Brand, "B_no", "B_name");
            ViewBag.C_no = new SelectList(db.Classfiy, "C_no", "C_name");
            return View();
        }

        // POST: Shangpins/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "S_no,S_name,S_number,C_no,B_no,S_costprice,S_marketprice,S_myprice,S_weight,S_count,S_max,S_min,S_flag,S_label,S_isHot,S_order")] Shangpin shangpin)
        {
            if (ModelState.IsValid)
            {
                db.Shangpin.Add(shangpin);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.B_no = new SelectList(db.Brand, "B_no", "B_name", shangpin.B_no);
            ViewBag.C_no = new SelectList(db.Classfiy, "C_no", "C_name", shangpin.C_no);
            return View(shangpin);
        }

        // GET: Shangpins/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shangpin shangpin = db.Shangpin.Find(id);
            if (shangpin == null)
            {
                return HttpNotFound();
            }
            ViewBag.B_no = new SelectList(db.Brand, "B_no", "B_name", shangpin.B_no);
            ViewBag.C_no = new SelectList(db.Classfiy, "C_no", "C_name", shangpin.C_no);
            return View(shangpin);
        }

        // POST: Shangpins/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "S_no,S_name,S_number,C_no,B_no,S_costprice,S_marketprice,S_myprice,S_weight,S_count,S_max,S_min,S_flag,S_label,S_isHot,S_order")] Shangpin shangpin)
        {
            if (ModelState.IsValid)
            {
                db.Entry(shangpin).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.B_no = new SelectList(db.Brand, "B_no", "B_name", shangpin.B_no);
            ViewBag.C_no = new SelectList(db.Classfiy, "C_no", "C_name", shangpin.C_no);
            return View(shangpin);
        }

        // GET: Shangpins/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shangpin shangpin = db.Shangpin.Find(id);
            if (shangpin == null)
            {
                return HttpNotFound();
            }
            return View(shangpin);
        }

        // POST: Shangpins/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Shangpin shangpin = db.Shangpin.Find(id);
            db.Shangpin.Remove(shangpin);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet, ActionName("addsc")]
        public ActionResult addsc(string id)
        {
            string str = Session["username"].ToString();
            db.shangpinshoucang.Add(new shangpinshoucang() { createtime = DateTime.Now, spid = id, username = str });
            db.SaveChanges();
            return Redirect("/spsc/Index");
        }
        public ActionResult aaaa()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
