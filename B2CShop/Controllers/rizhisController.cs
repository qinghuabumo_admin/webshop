﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using B2CShop.Models;

namespace B2CShop.Controllers
{
    public class rizhisController : Controller
    {
        private B2C db = new B2C();

        // GET: rizhis
        public ActionResult Index()
        {
            return View(db.rizhi.ToList());
        }

        // GET: rizhis/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            rizhi rizhi = db.rizhi.Find(id);
            if (rizhi == null)
            {
                return HttpNotFound();
            }
            return View(rizhi);
        }

        // GET: rizhis/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: rizhis/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,caozuo,createtime,username")] rizhi rizhi)
        {
            if (ModelState.IsValid)
            {
                db.rizhi.Add(rizhi);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(rizhi);
        }

        // GET: rizhis/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            rizhi rizhi = db.rizhi.Find(id);
            if (rizhi == null)
            {
                return HttpNotFound();
            }
            return View(rizhi);
        }

        // POST: rizhis/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,caozuo,createtime,username")] rizhi rizhi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rizhi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(rizhi);
        }

        // GET: rizhis/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            rizhi rizhi = db.rizhi.Find(id);
            if (rizhi == null)
            {
                return HttpNotFound();
            }
            return View(rizhi);
        }

        // POST: rizhis/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            rizhi rizhi = db.rizhi.Find(id);
            db.rizhi.Remove(rizhi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
