﻿using System;
using System.Collections.Generic;

namespace WebAppShop.Models
{
    public class PingLun
    {
        public string PL_no { get; set; }


        public string U_username { get; set; }


        public string S_no { get; set; }


        public string PL_content { get; set; }

        public DateTime PL_time { get; set; }

        public int PL_flag { get; set; }

    }
}