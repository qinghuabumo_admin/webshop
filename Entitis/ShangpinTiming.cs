﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppShop.Models
{
    /// <summary>
    /// 定时商品表
    /// </summary>
    public class ShangpinTiming
    {
        /// <summary>
        /// 定时商品编号
        /// </summary>
        public string ST_no { get; set; }

        /// <summary>
        /// 商品编号
        /// </summary>
        public string S_no { get; set; }

        /// <summary>
        /// 上架时间
        /// </summary>
        public DateTime ST_starttime { get; set; }

        /// <summary>
        /// 下架时间
        /// </summary>
        public DateTime ST_endtime { get; set; }
    }
}