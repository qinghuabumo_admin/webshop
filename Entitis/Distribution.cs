﻿using System;
using System.Collections.Generic;

namespace WebAppShop.Models
{
    public class Distribution
    {
        public string D_no { get; set; }

        public DateTime? D_time { get; set; }


        public decimal D_money { get; set; }


        public string D_methood { get; set; }

        
        
    }
}