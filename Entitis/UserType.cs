﻿using System.Collections.Generic;

namespace WebAppShop.Models
{
    public class UserType
    {
        public int UT_no { get; set; }


        public string UT_name { get; set; }


        public string UT_display { get; set; }


        public string UT_img { get; set; }

        public int UT_jfmax { get; set; }

        public int UT_jfmin { get; set; }

        public int UT_rank { get; set; }

        
    }
}