﻿using System.Collections.Generic;

namespace WebAppShop.Models
{
    public class UserAddress
    {
        public int UA_no { get; set; }


        public string UA_name { get; set; }


        public string U_username { get; set; }


        public string UA_person { get; set; }

        public int UA_main { get; set; }


        public string UA_tel { get; set; }

        public string UA_emailno { get; set; }

    }
}