﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppShop.Models
{
    /// <summary>
    /// 属性表分组表
    /// </summary>
    public class AttributeGroup
    {
        /// <summary>
        /// 属性分组编号
        /// </summary>
        public int AG_no { get; set; }

        /// <summary>
        /// 分组名称
        /// </summary>
        public string AG_name { get; set; }

        /// <summary>
        /// 分类编号
        /// </summary>
        public int C_no { get; set; }

        /// <summary>
        /// 排序值
        /// </summary>
        public int AG_order { get; set; }


    }
}