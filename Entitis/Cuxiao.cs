﻿using System;
using System.Collections.Generic;

namespace WebAppShop.Models
{
    public class Cuxiao
    {
        public string C_no { get; set; }

        public string S_no { get; set; }

        public DateTime C_starttime { get; set; }

        public DateTime C_endtime { get; set; }

        public int UT_no { get; set; }

        public int C_max { get; set; }

        public int C_flag { get; set; }

       
    }
}