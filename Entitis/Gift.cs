﻿namespace WebAppShop.Models
{
    /// <summary>
    /// 活动赠品表
    /// </summary>
    public class Gift
    {
        public int G_no { get; set; }

        public string C_no { get; set; }

        public string S_no { get; set; }

        public int G_count { get; set; }

        
    }
}