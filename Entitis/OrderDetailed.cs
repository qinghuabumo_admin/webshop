﻿using System.Collections.Generic;

namespace WebAppShop.Models
{
    public class OrderDetailed
    {
        public string OD_no { get; set; }


        public string S_no { get; set; }

        public int OD_count { get; set; }

        public string OD_number { get; set; }


        public string OD_attribute { get; set; }

        
        public decimal OD_price { get; set; }


        public string OD_name { get; set; }

        public string F_no { get; set; }

        public decimal OD_saleprice { get; set; }

        
    }
}