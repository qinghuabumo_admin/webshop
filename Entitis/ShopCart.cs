﻿namespace WebAppShop.Models
{
    public class ShopCart
    {
        public string SC_no { get; set; }


        public string U_username { get; set; }


        public string S_no { get; set; }

        public string SC_sname { get; set; }

        public decimal SC_sprice { get; set; }

        public int S_countmax { get; set; }

        public int S_count { get; set; }

       
    }
}