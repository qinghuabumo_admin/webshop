﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppShop.Models
{
    /// <summary>
    /// 品牌分类
    /// </summary>
    public class BrandClass
    {
        /// <summary>
        /// 品牌分类编号
        /// </summary>
        public int BC_no { get; set; }

        /// <summary>
        /// 品牌编号
        /// </summary>
        public int B_no { get; set; }

        /// <summary>
        /// 分类编号
        /// </summary>
        public int C_no { get; set; }


    }
}