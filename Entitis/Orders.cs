﻿using System;

namespace WebAppShop.Models
{
    public class Orders
    {
        public string O_no { get; set; }

        public DateTime O_time { get; set; }

        public string U_username { get; set; }

        public int O_flag { get; set; }


        public string D_no { get; set; }

        public string P_no { get; set; }

        public int UA_no { get; set; }

        public int? O_jfmoney { get; set; }


        public decimal O_sum { get; set; }

        public string OD_no { get; set; }

        public DateTime? O_goodtime { get; set; }

       
    }
}