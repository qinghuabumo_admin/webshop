﻿namespace WebAppShop.Models
{
    public class FavorableUser
    {
        public int FU_no { get; set; }

        public string F_no { get; set; }

        public string U_username { get; set; }

        public int FU_flag { get; set; }

        
    }
}