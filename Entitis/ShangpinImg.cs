﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppShop.Models
{
    /// <summary>
    /// 商品图片列表
    /// </summary>
    public class ShangpinImg
    {
        /// <summary>
        /// 商品图片编号
        /// </summary>
        public int SI_no { get; set; }

        /// <summary>
        /// 商品编号
        /// </summary>
        public string S_no { get; set; }

        /// <summary>
        /// 图片地址
        /// </summary>
        public string SI_img { get; set; }

        /// <summary>
        /// 是否为主图 1:是 2:否
        /// </summary>
        public int SI_main { get; set; }

        /// <summary>
        /// 排序值
        /// </summary>
        public int SI_order { get; set; }

        
    }
}