﻿using System;

namespace WebAppShop.Models
{
    public class HuiFu
    {
        public string H_no { get; set; }


        public string H_writer { get; set; }


        public string PL_no { get; set; }


        public string H_content { get; set; }

        public int H_flag { get; set; }

        public DateTime H_time { get; set; }

        
    }
}