﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppShop.Models
{
    /// <summary>
    /// 商品表
    /// </summary>
    public class Shangpin
    {
        /// <summary>
        /// 商品主键
        /// </summary>
        public string S_no { get; set; }

        /// <summary>
        /// 商品名称
        /// </summary>
        public string S_name { get; set; }

        /// <summary>
        /// 商品货号
        /// </summary>
        public string S_number { get; set; }

        /// <summary>
        /// 分类编号
        /// </summary>
        public int C_no { get; set; }

        /// <summary>
        /// 品牌编号
        /// </summary>
        public int B_no { get; set; }

        /// <summary>
        /// 成本价
        /// </summary>
        public decimal S_costprice { get; set; }

        /// <summary>
        /// 本店价
        /// </summary>
        public decimal S_myprice { get; set; }

        /// <summary>
        /// 重量
        /// </summary>
        public decimal S_weight { get; set; }

        /// <summary>
        /// 库存数量
        /// </summary>
        public int S_count { get; set; }

        /// <summary>
        /// 库存上限
        /// </summary>
        public int S_max { get; set; }

        /// <summary>
        /// 库存下线
        /// </summary>
        public int S_min { get; set; }

        /// <summary>
        /// 是否上架
        /// </summary>
        public int S_flag { get; set; }

        /// <summary>
        /// 商品标签
        /// </summary>
        public int S_label { get; set; }

        /// <summary>
        /// 是否热销
        /// </summary>
        public int S_isHot { get; set; }

        /// <summary>
        /// 排序值
        /// </summary>
        public int S_order { get; set; }

    }
}