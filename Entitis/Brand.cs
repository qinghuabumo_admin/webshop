﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppShop.Models
{
    /// <summary>
    /// 品牌
    /// </summary>
    public class Brand
    {
        /// <summary>
        /// 品牌编号
        /// </summary>
        public int B_no { get; set; }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string B_name { get; set; }

        /// <summary>
        /// 品牌Log
        /// </summary>
        public string B_Log { get; set; }

        /// <summary>
        /// 排序值
        /// </summary>
        public int B_order { get; set; }


    }
}