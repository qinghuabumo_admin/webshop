﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppShop.Models
{
    /// <summary>
    /// 关联商品
    /// </summary>
    public class ShangpinRelation
    {
        /// <summary>
        /// 关联商品编号
        /// </summary>
        public string SR_no { get; set; }

       /// <summary>
       /// 商品编号
       /// </summary>
        public string S_no { get; set; }

        /// <summary>
        /// 关联的商品编号
        /// </summary>
        public string SR_sno { get; set; }

       
    }
}