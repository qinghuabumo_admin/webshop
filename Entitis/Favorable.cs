﻿using System;
using System.Collections.Generic;

namespace WebAppShop.Models
{
    public class Favorable
    {
        public string F_no { get; set; }

        public string F_name { get; set; }

        public decimal F_money { get; set; }


        public decimal F_min { get; set; }

        public int F_count { get; set; }

        public int F_userCount { get; set; }

        public int F_getCount { get; set; }

        public DateTime F_userstarttime { get; set; }

        public DateTime F_userendtime { get; set; }

        public int F_flag { get; set; }

        
      
    }
}